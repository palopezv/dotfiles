# The Fuck settings file
#
# The rules are defined as in the example bellow:
#
# rules = ['cd_parent', 'git_push', 'python_command', 'sudo']
#
# The default values are as follows. Uncomment and change to fit your needs.
# See https://github.com/nvbn/thefuck#settings for more information.
#

# exclude_rules = []
# no_colors = False
# require_confirmation = True
# alter_history = True
# env = {'GIT_TRACE': '1', 'LANG': 'C', 'LC_ALL': 'C'}
# wait_command = 3
# priority = {}
# debug = False
# history_limit = None
# rules = [<const: All rules enabled>]
