# Path to your oh-my-zsh installation.
# export TERM=rxvt-unicode-256color
 export ZSH=/home/whoami/.oh-my-zsh
 export EDITOR=vim
 export VISUAL=vim
 export BROWSER=qutebrowser
 export GOPATH=$HOME/go
 export GOBIN=$GOPATH/bin
 export npm_config_prefix=~/.node_modules
 export LC_ALL=en_US.utf8
 export HASTE_SERVER="https://bin.fsociety.info"
 export QT_STYLE_OVERRIDE='gtk2'
 export QT_QPA_PLATFORMTHEME='gtk2'
# JIRA
 export JIRA_URL=http://192.168.0.56:8080
 export JIRA_NAME=deant
 export PATH="/usr/lib/ccache/bin/:$PATH"
# VULTR
 export GPG_TTY=$(tty)

 ## workaround for handling TERM variable in multiple tmux sessions properly from http://sourceforge.net/p/tmux/mailman/message/32751663/ by Nicholas Marriott
if [[ -n ${TMUX} && -n ${commands[tmux]}  ]];then
        case $(tmux showenv TERM 2>/dev/null) in
            *256color) ;&
            TERM=fbterm)
                    TERM=screen-256color ;;
            *)
                    TERM=screen
        esac
fi

# COLORS
# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
# ZSH_THEME="classyTouch"
ZSH_THEME="blinks"

#Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
#DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
export UPDATE_ZSH_DAYS=2

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git archlinux history-subsearch-string zsh-navigation-tools colorize screen)
# User configuration

export PATH="/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:$HOME/.gem/ruby/2.3.0/bin:$HOME/bin:$HOME/.node_modules/bin"
export PATH=$PATH:$GOBIN
export PATH="/usr/lib/ccache/bin/:$PATH"
export MANPATH="/usr/local/man:$MANPATH"
export CCACHE_DIR=/tmp/ccache
source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source $ZSH/oh-my-zsh.sh
source ~/.zsh_aliases
eval $(dircolors ~/.dircolors)
eval $(thefuck --alias)

c() {
    dir="$(dirlog-cd "$@")"
    if [ "$dir" != ""  ]; then
        cd "$dir" && ls
    fi
}
